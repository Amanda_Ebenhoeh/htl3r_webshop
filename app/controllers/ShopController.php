<?php

use Illuminate\Support\MessageBag;


class ShopController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		$question = Question::find(1);
		//dd($question->a1);

		return View::make('world', array('q' => $question));
	}

	public function showPrices(){
		$prices = Price::all();
		return View::make('prices', array('prices' => $prices));
	}

	public function showProducts($category = null){
		if(Auth::check())
		{
		if($category == null){
			$products = Product::all();
			$categories = Category::all();
			
			return View::make('products', array('products' => $products, 'categories' => $categories));
		}
		$products = Product::find(1); //categories ist die Funktion in Product.php
		$categories = Category::all();

		return View::make('products', array('products' => array($products), 'categories' => $categories));
		}
		else
		{
			return Redirect::to('/login');
		}

	}

	public function showDetails($artnr){
		$product = Product::find($artnr);
		$categories = Category::all();
		return View::make('details', array('product'=> $product, 'categories' => $categories));
		//return $product->eventname;

	}

	public function showWarenkorb($id){

		//$events = DB::table('warenkorb_events')->where('warenkorb_id','=', $id)->get();

		$warenkorb = Warenkorb::find($id); 
		
		return View::make('warenkorb', array('warenkorb'=> $warenkorb, 'categories'=> array()));

	}

	public function setWarenkorb($id){
		
		$user = Auth::user();

		if(Warenkorb::where('user_id', '=', $user->id)->count()>0){
			$warenkorb = Warenkorb::where('user_id', '=', $user->id)->first();

		}else{
			$warenkorb = new Warenkorb();
			
			$warenkorb->user_id = $user->id;
		}
		
		$warenkorb->save();

		DB::table('warenkorb_events')->insert(
			array('event_id' => $id, 'warenkorb_id' => $warenkorb->warenkorb_id));

		return Redirect::to('/events');

	}

	public function deleteWarenkorb($id){

		$user = Auth::user();
		$warenkorb = Warenkorb::where('user_id', '=', $user->id)->first();

		$warenkorb->events()->detach($id);

		$warenkorb->save();

		return Redirect::back();

	}

	public function loginB(){
		$username = Input::get('username');
		$password = Input::get('password');

		if(Auth::attempt(array('username' => $username, 'password' => $password)))
		{
			return Redirect::to('/events');
		}else{
			$error = new MessageBag(array('password'=>'Das von dir eingegebene Passwort ist falsch. Bitte versuche es noch einmal.'));
			return Redirect::back()->withInput()->withErrors($error);
		}
		die();
	}


	public function loginAction(){
	
		return View::make('login', array('categories'=>array()));
	}

	public function regist(){

		return View::make('registrierung', array('categories'=>array()));
	}

	public function registAction(){

		$validator = Validator::make(
	
			array(  'vorname' => Input::get('vorname'),
					'nachname' => Input::get('nachname'),
					'password' => Input::get('password'),
					'email' => Input::get('email'),
					'username' => Input::get('username')
				),

			array(  'vorname' => 'required|min:2|regex:/[A-Za-zäöü]/',
					'nachname' => 'required|min:2|regex:/[A-Za-zäöü]/',
					'password' => 'required|min:5',
					'email' => 'required|email',
					'username' => 'required|min:5'
			)
		);

		if($validator->fails()){
			return Redirect::back()->withErrors($validator);
		}
			
		$user = new User();
		$user->vorname = Input::get('vorname');
		$user->nachname = Input::get('nachname');
		$user->password = Hash::make(Input::get('password'));
		$user->email = Input::get('email');
		$user->username = Input::get('username');
		$user->save();
		return Redirect::to('/events');
	}

	public function logoutAction(){
		Auth::logout();
		return Redirect::to('/events');

	}

	public function adresse(){
		return View::make('adresse', array('categories'=>array()));	
	}

	public function adresseAction(){
		
		$validator = Validator::make(
	
			array(  'anrede' => Input::get('anrede'),
					'vorname' => Input::get('vorname'),
					'nachname' => Input::get('nachname'),
					'strasse' => Input::get('strasse'),
					'hausnummer' => Input::get('hausnummer'),
					'plz' => Input::get('plz'),
					'ort' => Input::get('ort'),
					'land' => Input::get('land')
				),

			array(  'anrede' => 'required|min:4|max:4|regex:/[A-Za-z]/',
					'vorname' => 'required|min:2|regex:/[A-Za-zäöü]/',
					'nachname' => 'required|min:2|regex:/[A-Za-zäöü]/',
					'strasse' => 'required|min:5|regex:/[A-Za-zäöü]/',
					'hausnummer' => 'required|numeric',
					'plz' => 'required|min:4|numeric',
					'ort' => 'required|min:3|regex:/[A-Za-zäöü]/',
					'land' => 'required|min:3|regex:/[A-Za-zäöüÖÜÄ]/'
			)
		);

		if($validator->fails()){
			return Redirect::back()->withErrors($validator);
		}

		$ladresse = new Lieferadresse();
		$ladresse->anrede = Input::get('anrede');
		$ladresse->liefer_vname = Input::get('vorname');
		$ladresse->liefer_nname = Input::get('nachname');
		$ladresse->strasse = Input::get('strasse');
		$ladresse->hausnummer = Input::get('hausnummer');
		$ladresse->plz = Input::get('plz');
		$ladresse->ort = Input::get('ort');
		$ladresse->land = Input::get('land');
		$ladresse->save();

		return Redirect::to('/zahlungsmethode');

	
	}

	public function uebersicht(){
		return View::make('uebersicht', array('categories'=>array()));	
	}

	public function zahlen(){
		return View::make('zahlungsmethode', array('categories'=>array()));	
	}


	public function newEvent(){
		return View::make(('neuesevent'), array('categories'=>array()));
	}

	public function newEventAction(){
		$validator = Validator::make(
	
			array(  'eventname' => Input::get('eventname'),
					'datum' => Input::get('datum'),
					'ort' => Input::get('ort'),
					'preis' => Input::get('preis'),
					'stueckzahl' => Input::get('stueckzahl'),
					'beginn' => Input::get('beginn'),
					'ende' => Input::get('ende'),
					'beschreibung' => Input::get('beschreibung')
				),

			array(  'eventname' => 'required|regex:/[A-Za-zäöüÖÜÄ]/',
					'datum' => 'required|date_format:Y-m-d',
					'ort' => 'required|min:2|regex:/[A-Za-zäöü]/',
					'preis' => 'required|numeric',
					'stueckzahl' => 'required|numeric',
					'beginn' => 'required|date_format:G:i:s',
					'ende' => 'required|date_format:G:i:s',
					'beschreibung' => 'min:3|regex:/[A-Za-zäöüÖÜÄ0-9]/'
			)
		);

		if($validator->fails()){
			return Redirect::back()->withErrors($validator);
		}

		$event = new Product();
		$event->eventname = Input::get('eventname');
		$event->datum = Input::get('datum');
		$event->ort = Input::get('ort');
		$event->preis = Input::get('preis');
		$event->stueckzahl = Input::get('stueckzahl');
		$event->uhrzeit_beginn = Input::get('beginn');
		$event->uhrzeit_ende = Input::get('ende');
		$event->beschreibung = Input::get('beschreibung');
		$event->save();

		return Redirect::to('/events');


	}

	public function adminLogin(){
		return View::make(('adminlogin'), array('categories'=>array()));
	}	

	public function adminLoginAction(){

		$username = Input::get('username');
		$password = Input::get('password');

		if(Auth::attempt(array('username' => $username, 'password' => $password)) && Auth::user()->admin == 1)
		{
			return Redirect::to('/neuesevent');
		}else{
			echo "Login fail";
		}
	}
		
	public function mailSenden(){

		$data = [];

		Mail::send('mail', $data, function($message)
		{
			$message->from('lisa.muellner96@gmx.at', 'Eventkauf.at');
   		 $message->to('amanda.ebenhoeh@aon.at', 'Amanda Ebenhöh')->subject('Bestätigungsmail');
		});
	}
	

}