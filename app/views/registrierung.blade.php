@extends('master')

@section('title')
	Registrierung
@stop

@section('h2')
	Registrierung
@stop

@section('content')


<div id="registrierbox">
	<form method="POST" action="/webshop/public/registrierung">
		<table id="table">
			<tr>
				<td>Vorname:</td>
				<td><input type="text" name="vorname" /> </td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('vorname') }}</span></td>
					@endif
			</tr>

			<tr>
				<td>Nachname:</td>
				<td><input type="text" name="nachname" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('nachname') }}</span></td>
					@endif
			</tr>

			<tr>
				<td>Email:</td>
				<td><input type="text" name="email" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('email') }}</span></td>
					@endif
			</tr>

			<tr>
				<td>Username:</td>
				<td><input type="text" name="username" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('username') }}</span></td>
					@endif
			</tr>

			<tr>
				<td>Passwort:</td>
				<td><input type="password" name="password" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('password') }}</span></td>
					@endif
			</tr>

			<tr>
				<td>Passwort wiederholen:</td>
				<td><input type="password" name="passwordw" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('passwordw') }}</span></td>
					@endif
			</tr>

			<tr>
				<td><input type="submit" name="action" value="Registrieren" class="btn btn-primary btn-sm"/></td>
			</tr>
		</table>
	</form>

</div>

@stop