@extends('master')

@section('title')
	Warenkorb
@stop

@section('h2')
	Warenkorb
@stop

@section('content')


<table class="table table-striped" id="table">
	<tr id="tr_headline">
		<td>Datum</td>
		<td>Name</td>
		<td>Ort</td>
		<td>Preis</td>
		<td>St&uuml;ckzahl</td>
		<td>Beginn</td>
		<td>Ende</td>
		<td></td>
	</tr>

	@foreach ($warenkorb->events as $event)
	<tr>
		<td>{{ $event->datum }} </td>
		<td>{{ $event->eventname	 }}</td>
		<td>{{ $event->ort }}</td>
		<td>{{ $event->preis }}€</td>
		<td>{{ $event->stueckzahl }} </td>
		<td>{{ $event->uhrzeit_beginn }}</td>
		<td>{{ $event->uhrzeit_ende }}</td>

		<td>
			{{ Form::open(array('url'=> '/warenkorb/'.$event->id, 'method'=>'delete')) }}
				<button type="submit" class="btn btn-primary btn-sm">Entfernen</button>
			{{ Form::close() }}
		</td>
	</tr>

	@endforeach

	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td><a href="./adresse"><button type="button" class="btn btn-primary btn-lg">Weiter</button></a></td>
	</tr>
</table>


@stop