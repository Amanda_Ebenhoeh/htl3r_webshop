@extends('master')

@section('title')
	Lieferadresse
@stop

@section('h2')
	Lieferadresse
@stop

@section('content')

<div id="registrierbox">
	<form method="POST" action="/webshop/public/adresse">
		<table id="addr_table">
			<tr>
				<td>Anrede:</td>
				<td><input type="text" name="anrede" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('anrede') }}</span></td>
					@endif
			</tr>

			<tr>
				<td>Vorname:</td>
				<td><input type="text" name="vorname" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('vorname') }}</span></td>
					@endif
		
			
				<td>Nachname:</td>
				<td><input type="text" name="nachname" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('nachname') }}</span></td>
					@endif
			</tr>

			<tr>
				<td>Straße:</td>
				<td><input type="text" name="strasse" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('strasse') }}</span></td>
					@endif
			

			
				<td>Hausnummer:</td>
				<td><input type="text" name="hausnummer" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('hausnummer') }}</span></td>
					@endif
			</tr>
		
			<tr>
				<td>Postleitzahl:</td>
				<td><input type="text" name="plz" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('plz') }}</span></td>
					@endif
			
				<td>Ort:</td>
				<td><input type="text" name="ort" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('ort') }}</span></td>
					@endif
			</tr>

			<tr>
				<td>Land:</td>
				<td><input type="text" name="land" /></td>
					@if($errors)
						<td><span style="color:red">{{ $errors->first('land') }}</span></td>
					@endif
			
		</table>

		<a href="./uebersicht"><input type="submit" class="btn btn-primary btn-sm" value="Weiter"/></a>
	</form>

</div>
<!-- value="Weiter" onclick="javascript: form.action='./zahlungsmethode'"-->

@stop