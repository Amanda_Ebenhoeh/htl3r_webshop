@extends('master')

@section('title')
	Events
@stop

@section('h2')
	Events
@stop
 

@section('content')
<form id="formi" method="POST">
<table class="table table-striped" id="table">
	<tr id="tr_headline">
		<td>Datum</td>
		<td>Name</td>
		<td>Ort</td>
		<td>Preis</td>
		<td>St&uuml;ckzahl</td>
		<td></td>
	</tr>

	@foreach ($products as $product)
	<tr>
		<td>{{  $product->datum }} </td>
		<td><a href="./details/{{ $product->id }}" >{{ $product->eventname }}</a> </td>
		<td>{{ $product->ort }}</td>
		<td>{{ $product->preis }}€</td>
		<td>{{ $product->stueckzahl }} </td>
		<td>
			<form id="formi2" action ="./warenkorb/{{ $product->id }}" method="POST">
				<button type="submit" class="btn btn-primary btn-sm">In den Warenkorb</button>
			</form>
		</td>
	</tr>
	@endforeach
</table>
</form>
@stop