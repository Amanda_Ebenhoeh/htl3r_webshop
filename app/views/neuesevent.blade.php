@extends('master')

@section('title')
	Event erstellen
@stop

@section('h2')
	Event erstellen
@stop

@section('content')

<div id="neueseventbox">
	<form method="POST" action="/webshop/public/neuesevent">
		<table id="table">
			<tr>
				<td><label for="event">Eventname</label></td>
    
				<td><input type="text" class="form-control" id="event" name="eventname"></td> </td>
				@if($errors)
						<td><span style="color:red">{{ $errors->first('eventname') }}</span></td>
				@endif
			</tr>

			<tr>
				<td><label>Datum</label></td>
				<td><input type="text" class="form-control" name="datum" placeholder="2013-08-30"></td>
				@if($errors)
						<td><span style="color:red">{{ $errors->first('datum') }}</span></td>
					@endif
			</tr>

			<tr>
				<td><label>Ort</label></td>
				<td><input type="text" class="form-control" name="ort"></td>
				@if($errors)
						<td><span style="color:red">{{ $errors->first('ort') }}</span></td>
					@endif
			</tr>

			<tr>
				<td><label>Preis</label></td>
				<td><input type="text" class="form-control" name="preis"></td>
				@if($errors)
						<td><span style="color:red">{{ $errors->first('preis') }}</span></td>
					@endif
			</tr>

			<tr>
				<td><label>St&uuml;ckzahl</td>
				<td><input type="text" class="form-control" name="stueckzahl"></td>
				@if($errors)
						<td><span style="color:red">{{ $errors->first('stueckzahl') }}</span></td>
					@endif
			</tr>

			<tr>
				<td><label>Beginn</label></td>
				<td><input type="text" class="form-control" name="beginn" placeholder="20:00:00"></td>
				@if($errors)
						<td><span style="color:red">{{ $errors->first('beginn') }}</span></td>
					@endif
			</tr>

			<tr>
				<td><label>Ende</label></td>
				<td><input type="text" class="form-control" name="ende" placeholder="23:00:00"></td>
				@if($errors)
						<td><span style="color:red">{{ $errors->first('ende') }}</span></td>
					@endif
			</tr>

			<tr>
				<td><label>Beschreibung</label></td>
				<td><input type="text" class="form-control" name="beschreibung"></td>
				@if($errors)
						<td><span style="color:red">{{ $errors->first('beschreibung') }}</span></td>
					@endif
			</tr>
			<tr>
			</tr>
			<tr>
				<td><input type="submit" name="action" value="Event erstellen" class="btn btn-primary btn-sm"/></td>
			</tr>
		</table>
	</form>

</div>

@stop