<!doctype html>
<html>
<head>
	<title>@yield('title')</title>
  {{ HTML::style('css/bootstrap.css') }}
  {{ HTML::style('css/produkte.css') }}

</head>

<body>   
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="">Webshop</a>
			</div>
      		<div class="collapse navbar-collapse navbar-ex1-collapse">
    			<ul class="nav navbar-nav">
      				<li class=""><a href="#">Home</a></li>
      				  <li class="dropdown">
        				  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Events <b class="caret"></b></a>
        				  <ul class="dropdown-menu">
        					   <h5>Kategorie</h5>
          				    <li><a href="./events/all">Alle</a></li>
          				    @foreach($categories as $category)
          				    <li><a href="./events/{{ $category->link }}">{{ $category->kategoriename }}</a></li>
          				    @endforeach
        				  </ul>
      				  </li>
         
              @if(Auth::check())
      				  <li class=""><a href="{{ action( 'ShopController@showWarenkorb',Auth::user()->warenkorb->warenkorb_id )}}"><span class="glyphicon glyphicon-shopping-cart"></span> Warenkorb</a></li>
              @endif
              <li class=""><a href="{{action('ShopController@loginB')}}">Login</a></li>
              <li class=""><a href="{{action('ShopController@logoutAction')}}">Logout</a></li>

      			</ul>
  			</div>
  		</div>
  	</nav>
      @if(Auth::check())
        <li class=""> Willkommen, {{Auth::user()->vorname }}. <a href="logout">Logout</a></li>
         
     @endif  

     
    

	<h2>@yield('h2')</h2>

	@yield('content')


	<div id="footer" class="bs-footer">
		<div id="container">
		</div>

	</div>

</body>
{{ HTML::script('js/jquery.js') }}
{{ HTML::script('js/bootstrap.js')}}


</html>
