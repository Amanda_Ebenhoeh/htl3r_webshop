@extends('master')

@section('title')
	Zahlungsmethode
@stop

@section('h2')
	Zahlungsmethode
@stop

@section('content')
Bitte w&auml;hlen Sie die gew&uuml;nschte Zahlungsmethode aus. <br />

<input type="radio" name="zahlungsart" value="Visa" date-toggle="collapse">Visa</input><br />

<input type="radio" name="zahlungsart" value="PayPal"/>PayPal<br />
<input type="radio" name="zahlungsart" value="Master Card"/>Master Card<br /><br />

<a href="./uebersicht"><button type="button" class="btn btn-primary btn-sm">Weiter</button></a>

<a href="./adresse"><button type="button" class="btn btn-primary btn-sm">Zur&uuml;ck</button></a>

{{ HTML::script('js/dropdown.js')}}
{{ HTML::script('js/bootstrap.js')}}

@stop