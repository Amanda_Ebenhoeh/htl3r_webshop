@extends('master')

@section('title')
	Login
@stop

@section('h2')
	Login
@stop

@section('content')


<div id="loginbox">
	<form method="POST" action="/webshop/public/login">
		<table id="table">
			<tr>
				<td>Username:</td>
				<td><input type="text" name="username" /></td>
			</tr>

			<tr>
				<td>Passwort:</td>
				<td><input type="password" name="password" /></td>
			</tr>

			<tr>
				<td colspan="2">
					@if($errors)
						{{ implode('', $errors->all(':message')) }}
					@endif
				</td>
			</tr>

			<tr>
				<td><input type="submit" name="action" value="Anmelden" class="btn btn-primary btn-sm" /></td>
			</tr>
		</table>
	</form>

</div>

@stop