@extends('master')

@section('title')
	Eventdetails
@stop

@section('h2')
	Eventdetails
@stop

@section('content')

<div id="eventdetails">
	<ul id="ul_detailliste">
	<li><span id="span_details_headline">{{ $product->eventname}} am {{ $product->datum }}</span></li><br />
	<li><span>Von: {{ $product->uhrzeit_beginn}} Uhr bis  {{ $product->uhrzeit_ende }} Uhr </span></li><br />
	<li><span>Ort: {{ $product->ort }}</span></li><br />
	<li><span>Preis pro Person: {{ $product->preis }}€ </span></li><br />
	</ul>

</div>
@stop