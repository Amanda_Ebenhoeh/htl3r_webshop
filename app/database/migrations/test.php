<?php

class DatabaseSeeder extends Seeder{
	/**
	*Run the migrations.
	*
	*@return void
	*/	

	public function up(){

		Schema::create('tasks', function(Blueprint $table){
			$table->increments(''id);
			$table->timestamps();

			$table->string('title');
			$table->string('description');
		});
	}

	/**
	*Reverse the migrations.
	*
	*@return void
	*/	

	public function down(){
		
		Schema::drop('tasks');
	}
}