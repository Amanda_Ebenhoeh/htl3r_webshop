<?php

class Warenkorb extends Eloquent 
{


	protected $table = 'warenkorb';

	protected $primaryKey = 'warenkorb_id'; 

	public function events(){
		return $this->belongsToMany('Product', 'warenkorb_events', 'warenkorb_id', 'event_id');
	}


}