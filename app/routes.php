<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

//Route::get('/', 'HomeController@showWelcome');

Route::get('/fff', 'HomeController@showPrices');

//Route::post('/registrierung', 'ShopController@check_name');

Route::get('/events/{category?}', 'ShopController@showProducts'); //? weil der Parameter optional ist

Route::get('/details/{artnr}', 'ShopController@showDetails');

Route::get('/warenkorb/{id}', 'ShopController@showWarenkorb');

Route::post('/warenkorb/{id}', 'ShopController@setWarenkorb');

Route::get('/login', 'ShopController@loginAction');

Route::post('/login', 'ShopController@loginB');

Route::post('/registrierung', 'ShopController@registAction');

Route::get('/registrierung', 'ShopController@regist');

Route::get('/logout', 'ShopController@logoutAction');

Route::delete('/warenkorb/{id}', 'ShopController@deleteWarenkorb');

Route::post('/adresse', 'ShopController@adresseAction');
Route::get('/adresse', 'ShopController@adresse');


Route::get('/uebersicht', 'ShopController@uebersicht');

Route::get('/kassa', 'ShopController@kassa');

Route::get('/zahlungsmethode', 'ShopController@zahlen');

Route::get('/admin', 'ShopController@adminLogin');

Route::post('/admin', 'ShopController@adminLoginAction');

Route::get('/neuesevent', 'ShopController@newEvent');

Route::post('/neuesevent', 'ShopController@newEventAction');
